<?php
require_once "./code.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SO1 Activity: PHP Basics and Selection Control</title>
</head>

<body>
    <h2>Full Address</h2>
    <p>
        <?php echo getFullAddress('3F Caswynn Bldg., Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?>
    </p>
    <p>
        <?php echo getFullAddress('3F Enzo Bldg., Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?>
    </p>

    <h2>Letter-Based Grading</h2>
    <p>
        <?php echo "87 is equivalent to " . getLetterGrade(87); ?>
        <br>
        <?php echo "94 is equivalent to " . getLetterGrade(94); ?>
        <br>
        <?php echo "74 is equivalent to " . getLetterGrade(74); ?>
    </p>

</body>

</html>