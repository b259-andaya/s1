<?php
function getFullAddress($specificAddress, $city, $province, $country)
{
    return "$specificAddress, $city, $province, $country";
}

function getLetterGrade($grades)
{
    if ($grades == 98 || $grades == 99 || $grades == 100) {
        return 'A+';
    } else if ($grades == 95 || $grades == 96 || $grades == 97) {
        return 'A';
    } else if ($grades == 92 || $grades == 93 || $grades == 94) {
        return 'A-';
    } else if ($grades == 89 || $grades == 90 || $grades == 91) {
        return 'B+';
    } else if ($grades == 86 || $grades == 87 || $grades == 88) {
        return 'B';
    } else if ($grades == 83 || $grades == 84 || $grades == 85) {
        return 'B-';
    } else if ($grades == 80 || $grades == 81 || $grades == 82) {
        return 'C+';
    } else if ($grades == 77 || $grades == 78 || $grades == 79) {
        return 'C';
    } else if ($grades == 75 || $grades == 76) {
        return 'C-';
    } else {
        return 'F';
    }
}
